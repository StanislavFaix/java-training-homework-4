package com.faix.stanislav.weather.pack;

import java.util.HashMap;
import java.util.Map;

public class CitiesWind {

	private Map<String, String> windDescription = new HashMap<String, String>();
	
	public Map<String, String> getCities() {
		return windDescription;
	}
	
	public void addWindDescription(String city, String description) {
		windDescription.put(city, description);
	}
}
