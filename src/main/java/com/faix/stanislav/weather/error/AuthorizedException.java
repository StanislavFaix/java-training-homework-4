package com.faix.stanislav.weather.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Invalid authentication")
public class AuthorizedException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public AuthorizedException() {
		super();
	}
}
