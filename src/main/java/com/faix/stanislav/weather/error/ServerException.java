package com.faix.stanislav.weather.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server error")
public class ServerException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public ServerException() {
		super();
	}
}
