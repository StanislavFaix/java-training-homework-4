package com.faix.stanislav.weather.cache;

import java.io.InputStream;
import java.util.Properties;

public class WundergroundProperties {

	private Properties properties = new Properties();
	
	public void loadProperties(String fileName) throws Exception {
		InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
		properties.load(input);
	}
	
	public String getLink() {
		return properties.getProperty("wunderground.link");
	}
	
	public String getApiKey() {
		return properties.getProperty("wunderground.apikey");
	}
}
