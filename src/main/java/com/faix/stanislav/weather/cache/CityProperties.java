package com.faix.stanislav.weather.cache;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.faix.stanislav.weather.pack.City;

public class CityProperties {

	private Properties properties = new Properties();
	
	private List<City> cities = new ArrayList<City>();
	
	public void loadProperties(String fileName) throws Exception {
		cities.clear();
		InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
		properties.load(input);		
		String valueString = properties.getProperty("cities.names");
		String[] values = valueString.split(";");	
		
		for(String s: values) {
			String[] value = s.split(":");
			if(value.length == 2) {
				cities.add(new City(value[0], value[1]));
			}
		}	
	}
	
	public City[] getCities() {
		City[] cityList = new City[cities.size()];
		cities.toArray(cityList);
		return cityList;
	}
	
	public boolean containsCity(String city) {		
		for(City c: cities) {
			if(c.getName().toLowerCase().equals(city)) {
				return true;
			}
		}
		return false;
	}
	
	public City getCity(String city) {
		for(City c: cities) {
			if(c.getName().toLowerCase().equals(city)) {
				return c;
			}
		}
		return null;
	}
}
