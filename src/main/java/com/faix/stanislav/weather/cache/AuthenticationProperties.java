package com.faix.stanislav.weather.cache;

import java.io.InputStream;
import java.util.Properties;

public class AuthenticationProperties {

	private Properties properties = new Properties();
	
	public void loadProperties(String fileName) throws Exception {
		InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
		properties.load(input);
	}
	
	public String getUserName() {
		return properties.getProperty("authentication.username");
	}
	
	public String getPassword() {
		return properties.getProperty("authentication.password");
	}
}
