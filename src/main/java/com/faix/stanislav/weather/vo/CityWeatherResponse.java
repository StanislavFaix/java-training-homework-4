package com.faix.stanislav.weather.vo;

import com.faix.stanislav.weather.pack.CityWeather;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class CityWeatherResponse {

	private CityWeather cityWeather;
	
	public CityWeatherResponse() {
		cityWeather = new CityWeather();
	}
	
	public CityWeatherResponse(CityWeather cityWeather) {
		this.cityWeather = cityWeather;
	}
	
	public CityWeather getCityWeather() {
		return cityWeather;
	}
	
	@Override
	public String toString() {
		
		JsonNodeFactory nodeFactory = new JsonNodeFactory(true);
		ObjectNode node = nodeFactory.objectNode();
		node.put("location", cityWeather.getLocation());
		node.put("observation_time", cityWeather.getObservation());
		node.put("temperature", cityWeather.getTemperature());
		node.put("humidity", cityWeather.getHumidity());
		node.put("wind_speed", cityWeather.getWindSpeed());
		node.put("wind_direction", cityWeather.getWindDirection());
		node.put("weather_description", cityWeather.getWeatherDescription());
		node.put("wind_description", cityWeather.getWindDescription());		
		
		return node.toString();
	}
}
