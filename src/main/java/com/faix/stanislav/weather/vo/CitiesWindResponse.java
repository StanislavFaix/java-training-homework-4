package com.faix.stanislav.weather.vo;

import com.faix.stanislav.weather.pack.CitiesWind;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class CitiesWindResponse {

	private CitiesWind citiesWind;
	
	public CitiesWindResponse() {
		citiesWind = new CitiesWind();
	}
	
	public CitiesWindResponse(CitiesWind citiesWind) {
		this.citiesWind = citiesWind;
	}
	
	public CitiesWind getCityWeather() {
		return citiesWind;
	}
	
	@Override
	public String toString() {
		
		JsonNodeFactory nodeFactory = new JsonNodeFactory(true);
		ObjectNode node = nodeFactory.objectNode();
		
		for(String city: citiesWind.getCities().keySet()) {
			node.put(city, citiesWind.getCities().get(city));
		}		
		return node.toString();
	}
}
