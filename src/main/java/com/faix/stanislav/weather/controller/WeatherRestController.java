package com.faix.stanislav.weather.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.faix.stanislav.weather.cache.CityProperties;
import com.faix.stanislav.weather.error.AuthorizedException;
import com.faix.stanislav.weather.error.ServerException;
import com.faix.stanislav.weather.pack.City;
import com.faix.stanislav.weather.service.AuthenticationService;
import com.faix.stanislav.weather.service.WeatherService;
import com.faix.stanislav.weather.service.WundergroundService;
import com.faix.stanislav.weather.vo.CitiesWindResponse;
import com.faix.stanislav.weather.vo.CityWeatherResponse;

@RestController
@SpringBootApplication
@EnableScheduling
public class WeatherRestController {
	
	private WundergroundService wundergroundService;
	
	private WeatherService weatherService;
	
	private AuthenticationService authenticationService;
	
	public WeatherRestController() {
		wundergroundService = new WundergroundService();
		weatherService = new WeatherService();
		authenticationService = new AuthenticationService();
	}
	
	@RequestMapping(value = "/weather", method = RequestMethod.GET)
	public String windList(@RequestHeader(value = "Authorization") String authorization) throws Exception{
		
		if(!authenticationService.authenticate(authorization)) {
			throw new AuthorizedException();
		}
		CityProperties cityProperties = new CityProperties();
		
		try {
			cityProperties.loadProperties("cities.properties");
		} catch(Exception e) {
			throw new ServerException();
		}
		
		CitiesWindResponse response = new CitiesWindResponse();		
		for(City city: cityProperties.getCities()) {
			response.getCityWeather().addWindDescription(city.getName(),
					weatherService.getWindDescription(city.getName()));
		}		
		return response.toString();
	}
	
	@RequestMapping(value = "/weather/{city}", method = RequestMethod.GET)
	public String locationData(@PathVariable String city,
			@RequestHeader(value = "Authorization") String authorization) {
		
		if(!authenticationService.authenticate(authorization)) {
			throw new AuthorizedException();
		}
		
		CityWeatherResponse response = new CityWeatherResponse(weatherService.getCityWeather(city));		
		return response.toString();
	}
	
	@Scheduled(fixedRate = 1800000)
	public void wunderground() {		
		
		CityProperties cityProperties = new CityProperties();
		
		try {
			cityProperties.loadProperties("cities.properties");		
			for(City city: cityProperties.getCities()) {
				wundergroundService.downloadData(city.getCountry(), city.getName());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main( String[] args )
    {
		SpringApplication.run(WeatherRestController.class, args);
    }    
}
