package com.faix.stanislav.weather.service;

import java.util.Base64;

import com.faix.stanislav.weather.cache.AuthenticationProperties;
import com.faix.stanislav.weather.error.ServerException;

public class AuthenticationService {

	private AuthenticationProperties authenticationProperties;
	
	public AuthenticationService() {
		authenticationProperties = new AuthenticationProperties();
	}
	
	public Boolean authenticate(String header) throws ServerException {
		
		try {
			authenticationProperties.loadProperties("authentication.properties");
		} catch(Exception e) {
			throw new ServerException();
		}
		String username = authenticationProperties.getUserName();
		String password = authenticationProperties.getPassword();		
		return Base64.getEncoder().encodeToString((username + ":" + password).getBytes()).equals(header);
	}
}
