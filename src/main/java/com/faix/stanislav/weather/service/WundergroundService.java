package com.faix.stanislav.weather.service;

import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;

import com.faix.stanislav.weather.cache.WundergroundProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class WundergroundService {

	private WundergroundProperties wundergroundProperties;
	
	public WundergroundService() {
		wundergroundProperties = new WundergroundProperties();
	}
	
	public void downloadData(String country, String city) throws Exception {
		
		wundergroundProperties.loadProperties("wunderground.properties");
		
		String url = wundergroundProperties.getLink()
				.replaceAll("\\{apiKey\\}", wundergroundProperties.getApiKey())
				.replaceAll("\\{country\\}", country)
				.replaceAll("\\{city\\}", city);
		
		ObjectMapper objectMapper = new ObjectMapper();
		InputStream is = new URL(url).openStream();
		JsonNode node = objectMapper.readTree(is);	
		is.close();
		((ObjectNode) node).put("timestamp", System.currentTimeMillis());

		PrintWriter writer = new PrintWriter(this.getClass().getClassLoader().getResource("").getPath() + city + ".json", "UTF-8");
		writer.println(node.toString());
		writer.close();
	}
}
