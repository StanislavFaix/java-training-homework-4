package com.faix.stanislav.weather.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.faix.stanislav.weather.error.ServerException;
import com.faix.stanislav.weather.pack.CityWeather;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class WeatherService {
	
	public CityWeather getCityWeather(String city) throws ServerException {
		
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode node;
		JsonNode currentObservation;
		
		try {
			InputStream is = new FileInputStream(this.getClass().getClassLoader().
					getResource("").getPath() + city + ".json");
			node = objectMapper.readTree(is);
			currentObservation = node.path("current_observation");
			is.close();	
		} catch(Exception e) {
			throw new ServerException();
		}
		
		CityWeather cityWeather = new CityWeather();		
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));       
		cityWeather.setObservation(sdf.format(new Date(new Timestamp(node.path("timestamp").asLong()).getTime())));
		cityWeather.setLocation(currentObservation.path("display_location")
				.path("full").asText());
		cityWeather.setTemperature(currentObservation.path("temp_c").asDouble());
		cityWeather.setHumidity(currentObservation.path("relative_humidity").asText());
		cityWeather.setWindSpeed(currentObservation.path("wind_kph").asText());
		cityWeather.setWindDirection(currentObservation.path("wind_dir").asText());
		cityWeather.setWeatherDescription(currentObservation.path("weather").asText());
		cityWeather.setWindDescription(currentObservation.path("wind_string").asText());
		
		return cityWeather;
	}
	
	public String getWindDescription(String city) throws ServerException {
		
		String windDescription = new String();
		
		try{
			ObjectMapper objectMapper = new ObjectMapper();		
			InputStream is = new FileInputStream(this.getClass().getClassLoader().
					getResource("").getPath() + city + ".json");
			
			windDescription = objectMapper.readTree(is).path("current_observation").path("wind_string").asText();
			
			is.close();
		} catch(Exception e) {
			throw new ServerException();
		}
		
		return windDescription;
	}
}
